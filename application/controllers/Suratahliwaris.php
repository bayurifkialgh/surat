<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Suratahliwaris extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index() {
		$judul = [
			'title' => 'Management Surat',
			'sub_title' => 'Surat Ahli Waris'
		];

		$data['data'] = $this->db->get('surat_ahli_waris')->result_array();

		$this->load->view('templates/header', $judul);
		$this->load->view('surat/suratahliwaris', $data);
		$this->load->view('templates/footer');
	}

	// Create action
	public function create() {
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('isi_surat', 'Isi Surat', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
		if (empty($_FILES['foto']['name']))
		{
			$this->form_validation->set_rules('foto', 'Foto', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Surat Ahli Waris'
			];
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/tambah_surat_ahliwaris');
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} else {
				echo "<script>alert('Gagal Upload Foto!')</script>";
				redirect(base_url("suratahliwaris/create"));
			}

			$this->db->insert('surat_ahli_waris', $post);
			$this->session->set_flashdata('success', 'Berhasil Ditambahkan!');
			redirect(base_url("suratahliwaris"));
		}
	}

	// Update action
	public function update($id) {
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('isi_surat', 'Isi Surat', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Surat Ahli Waris'
			];
			$data['data'] = $this->db->get_where('surat_ahli_waris', ['id' => $id])->row_array();
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/edit_surat_ahliwaris', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} 

			$this->db->update('surat_ahli_waris', $post, ['id' => $id]);
			$this->session->set_flashdata('success', 'Berhasil Diubah!');
			redirect(base_url("suratahliwaris"));
		}
	}

	// Delete action
	public function delete($id) {
		$this->db->delete('surat_ahli_waris', ['id' => $id]);
		$this->session->set_flashdata('success', 'Berhasil Dihapus!');
		redirect(base_url("suratahliwaris"));
	}
}
