<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model', 'dashboard');
        if ($this->session->userdata('id_user') == FALSE) {
            redirect(base_url("auth/login"));
        }
    }

    public function index()
    {
        $judul = [
            'title' => 'Dashboard',
            'sub_title' => ''
        ];

        // $data['sm'] = $this->db->get('surat_masuk')->row_array();
        $januari = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="1"')->num_rows();
        $februari = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="2"')->num_rows();
        $maret = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="3"')->num_rows();
        $april = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="4"')->num_rows();
        $mei = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="5"')->num_rows();
        $juni = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="6"')->num_rows();
        $juli = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="7"')->num_rows();
        $agustus = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="8"')->num_rows();
        $september = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="9"')->num_rows();
        $oktober = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="10"')->num_rows();
        $november = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="11"')->num_rows();
        $desember = $this->db->query('SELECT * FROM surat_masuk WHERE month(tanggal)="12"')->num_rows();


        $januari1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="1"')->num_rows();
        $februari1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="2"')->num_rows();
        $maret1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="3"')->num_rows();
        $april1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="4"')->num_rows();
        $mei1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="5"')->num_rows();
        $juni1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="6"')->num_rows();
        $juli1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="7"')->num_rows();
        $agustus1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="8"')->num_rows();
        $september1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="9"')->num_rows();
        $oktober1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="10"')->num_rows();
        $november1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="11"')->num_rows();
        $desember1 = $this->db->query('SELECT * FROM surat_keluar WHERE month(tanggal)="12"')->num_rows();


        $januari2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="1"')->num_rows();
        $februari2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="2"')->num_rows();
        $maret2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="3"')->num_rows();
        $april2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="4"')->num_rows();
        $mei2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="5"')->num_rows();
        $juni2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="6"')->num_rows();
        $juli2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="7"')->num_rows();
        $agustus2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="8"')->num_rows();
        $september2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="9"')->num_rows();
        $oktober2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="10"')->num_rows();
        $november2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="11"')->num_rows();
        $desember2 = $this->db->query('SELECT * FROM surat_keterangan WHERE month(tanggal_surat_keterangan)="12"')->num_rows();

		$januari3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="1"')->num_rows();
        $februari3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="2"')->num_rows();
        $maret3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="3"')->num_rows();
        $april3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="4"')->num_rows();
        $mei3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="5"')->num_rows();
        $juni3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="6"')->num_rows();
        $juli3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="7"')->num_rows();
        $agustus3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="8"')->num_rows();
        $september3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="9"')->num_rows();
        $oktober3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="10"')->num_rows();
        $november3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="11"')->num_rows();
        $desember3 = $this->db->query('SELECT * FROM surat_sktm WHERE month(tanggal)="12"')->num_rows();

		$januari4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="1"')->num_rows();
        $februari4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="2"')->num_rows();
        $maret4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="3"')->num_rows();
        $april4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="4"')->num_rows();
        $mei4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="5"')->num_rows();
        $juni4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="6"')->num_rows();
        $juli4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="7"')->num_rows();
        $agustus4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="8"')->num_rows();
        $september4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="9"')->num_rows();
        $oktober4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="10"')->num_rows();
        $november4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="11"')->num_rows();
        $desember4 = $this->db->query('SELECT * FROM surat_ahli_waris WHERE month(tanggal)="12"')->num_rows();

		// kartu_kuning
		$januari5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="1"')->num_rows();
		$februari5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="2"')->num_rows();
		$maret5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="3"')->num_rows();
		$april5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="4"')->num_rows();
		$mei5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="5"')->num_rows();
		$juni5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="6"')->num_rows();
		$juli5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="7"')->num_rows();
		$agustus5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="8"')->num_rows();
		$september5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="9"')->num_rows();
		$oktober5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="10"')->num_rows();
		$november5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="11"')->num_rows();
		$desember5 = $this->db->query('SELECT * FROM kartu_kuning WHERE month(tanggal)="12"')->num_rows();

		// Table dispensasinikah where month(tanggal_pemohonan)
		$januari6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="1"')->num_rows();
		$februari6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="2"')->num_rows();
		$maret6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="3"')->num_rows();
		$april6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="4"')->num_rows();
		$mei6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="5"')->num_rows();
		$juni6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="6"')->num_rows();
		$juli6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="7"')->num_rows();
		$agustus6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="8"')->num_rows();
		$september6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="9"')->num_rows();
		$oktober6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="10"')->num_rows();
		$november6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="11"')->num_rows();
		$desember6 = $this->db->query('SELECT * FROM dispensasinikah WHERE month(tanggal_pemohonan)="12"')->num_rows();
		

        $data['masuk'] = [$januari, $februari, $maret, $april, $mei, $juni, $juli, $agustus, $september, $oktober, $november, $desember];

        $data['keluar'] = [$januari1, $februari1, $maret1, $april1, $mei1, $juni1, $juli1, $agustus1, $september1, $oktober1, $november1, $desember1];

        $data['keterangan'] = [$januari2, $februari2, $maret2, $april2, $mei2, $juni2, $juli2, $agustus2, $september2, $oktober2, $november2, $desember2];

		$data['sktm'] = [$januari3, $februari3, $maret3, $april3, $mei3, $juni3, $juli3, $agustus3, $september3, $oktober3, $november3, $desember3];

		$data['ahli_waris'] = [$januari4, $februari4, $maret4, $april4, $mei4, $juni4, $juli4, $agustus4, $september4, $oktober4, $november4, $desember4];

		$data['kartu_kuning'] = [$januari5, $februari5, $maret5, $april5, $mei5, $juni5, $juli5, $agustus5, $september5, $oktober5, $november5, $desember5];

		$data['dispensasinikah'] = [$januari6, $februari6, $maret6, $april6, $mei6, $juni6, $juli6, $agustus6, $september6, $oktober6, $november6, $desember6];




        // $data["smm"] = $this->db->get('surat_masuk');
        // $data["skk"] = $this->db->get('surat_keluar');

        // var_dump($data);


        $data2['sm'] = $this->db->query('SELECT tanggal FROM surat_masuk ORDER BY tanggal DESC LIMIT 1')->result_array();

        $data2['sk'] = $this->db->query('SELECT tanggal FROM surat_keluar ORDER BY tanggal DESC LIMIT 1')->result_array();

        $data2['sket'] = $this->db->query('SELECT tanggal_surat_keterangan FROM surat_keterangan ORDER BY tanggal_surat_keterangan DESC LIMIT 1')->result_array();
		
        $data2['skm'] = $this->db->query('SELECT tanggal FROM surat_sktm ORDER BY tanggal DESC LIMIT 1')->result_array();

		$data2['ah'] = $this->db->query('SELECT tanggal FROM surat_ahli_waris ORDER BY tanggal DESC LIMIT 1')->result_array();

		$data2['kk'] = $this->db->query('SELECT tanggal FROM kartu_kuning ORDER BY tanggal DESC LIMIT 1')->result_array();

		$data2['dp'] = $this->db->query('SELECT tanggal_pemohonan FROM dispensasinikah ORDER BY tanggal_pemohonan DESC LIMIT 1')->result_array();

        if ($data2['sm'] == null) {
            $data2['sm'] = 0;
        }
        if($data2['sk'] == null){
            $data2['sk'] = 0;
        }
        if($data2['sket'] == null){
            $data2['sket'] = 0;
        }

		if($data2['skm'] == null){
			$data2['skm'] = 0;
		}

		if($data2['ah'] == null){
			$data2['ah'] = 0;
		}

		if($data2['kk'] == null){
			$data2['kk'] = 0;
		}

		if($data2['dp'] == null){
			$data2['dp'] = 0;
		}

        // var_dump($data2);
        // die;

        $this->load->view('templates/header', $judul);
        $this->load->view('dashboard/index', $data2);
        $this->load->view('templates/footer', $data);
    }
}
