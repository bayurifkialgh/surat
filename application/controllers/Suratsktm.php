<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Suratsktm extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index() {
		$judul = [
			'title' => 'Management Surat',
			'sub_title' => 'Surat SKTM'
		];

		$data['data'] = $this->db->get('surat_sktm')->result_array();

		$this->load->view('templates/header', $judul);
		$this->load->view('surat/sktm', $data);
		$this->load->view('templates/footer');
	}

	// Create action
	public function create() {
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('isi_surat', 'Isi Surat', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
		if (empty($_FILES['foto']['name']))
		{
			$this->form_validation->set_rules('foto', 'Foto', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Surat SKTM'
			];
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/tambah_surat_sktm');
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} else {
				echo "<script>alert('Gagal Upload Foto!')</script>";
				redirect(base_url("suratsktm/create"));
			}

			$this->db->insert('surat_sktm', $post);
			$this->session->set_flashdata('success', 'Berhasil Ditambahkan!');
			redirect(base_url("suratsktm"));
		}
	}

	// Update action
	public function update($id) {
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('isi_surat', 'Isi Surat', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Surat SKTM'
			];
			$data['data'] = $this->db->get_where('surat_sktm', ['id' => $id])->row_array();
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/edit_surat_sktm', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} 

			$this->db->update('surat_sktm', $post, ['id' => $id]);
			$this->session->set_flashdata('success', 'Berhasil Diubah!');
			redirect(base_url("suratsktm"));
		}
	}

	// Delete action
	public function delete($id) {
		$this->db->delete('surat_sktm', ['id' => $id]);
		$this->session->set_flashdata('success', 'Berhasil Dihapus!');
		redirect(base_url("suratsktm"));
	}
}
