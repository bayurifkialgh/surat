<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kartukuning extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index() {
		$judul = [
			'title' => 'Management Surat',
			'sub_title' => 'Kartu Kuning'
		];

		$data['data'] = $this->db->get('kartu_kuning')->result_array();

		$this->load->view('templates/header', $judul);
		$this->load->view('surat/kartukuning', $data);
		$this->load->view('templates/footer');
	}

	// Create action
	public function create() {
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'trim|required');
		$this->form_validation->set_rules('tahun_lulus', 'Tahun Lulus', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		if (empty($_FILES['foto']['name']))
		{
			$this->form_validation->set_rules('foto', 'Foto', 'required');
		}


		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Kartu Kuning'
			];
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/tambah_surat_kartukuning');
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} else {
				echo "<script>alert('Gagal Upload Foto!')</script>";
				redirect(base_url("kartukuning/create"));
			}

			$this->db->insert('kartu_kuning', $post);
			$this->session->set_flashdata('success', 'Berhasil Ditambahkan!');
			redirect(base_url("kartukuning"));
		}
	}

	// Update action
	public function update($id) {
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'trim|required');
		$this->form_validation->set_rules('tahun_lulus', 'Tahun Lulus', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Kartu Kuning'
			];
			$data['data'] = $this->db->get_where('kartu_kuning', ['id' => $id])->row_array();
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/edit_surat_kartukuning', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} 

			$this->db->update('kartu_kuning', $post, ['id' => $id]);
			$this->session->set_flashdata('success', 'Berhasil Diubah!');
			redirect(base_url("kartukuning"));
		}
	}

	// Delete action
	public function delete($id) {
		$this->db->delete('kartu_kuning', ['id' => $id]);
		$this->session->set_flashdata('success', 'Berhasil Dihapus!');
		redirect(base_url("kartukuning"));
	}
}
