<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dispensasinikah extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index() {
		$judul = [
			'title' => 'Management Surat',
			'sub_title' => 'Dispensasi Nikah'
		];

		$data['data'] = $this->db->get('dispensasinikah')->result_array();

		$this->load->view('templates/header', $judul);
		$this->load->view('surat/dispensasinikah', $data);
		$this->load->view('templates/footer');
	}

	// Create action
	public function create() {
		$this->form_validation->set_rules('tanggal_pemohonan', 'Tanggal Pemohonan', 'trim|required');
		$this->form_validation->set_rules('nomer_surat', 'Nomer Surat', 'trim|required');
		$this->form_validation->set_rules('calon_lakilaki', 'Calon Lakilaki', 'trim|required');
		$this->form_validation->set_rules('calon_perempuan', 'Calon Perempuan', 'trim|required');
		$this->form_validation->set_rules('tanggal_penikahan', 'Tanggal Penikahan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
		if (empty($_FILES['foto']['name']))
		{
			$this->form_validation->set_rules('foto', 'Foto', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Dispensasi Nikah'
			];
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/tambah_surat_dispensasinikah');
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} else {
				echo "<script>alert('Gagal Upload Foto!')</script>";
				redirect(base_url("dispensasinikah/create"));
			}

			$this->db->insert('dispensasinikah', $post);
			$this->session->set_flashdata('success', 'Berhasil Ditambahkan!');
			redirect(base_url("dispensasinikah"));
		}
	}

	// Update action
	public function update($id) {
		$this->form_validation->set_rules('tanggal_pemohonan', 'Tanggal Pemohonan', 'trim|required');
		$this->form_validation->set_rules('nomer_surat', 'Nomer Surat', 'trim|required');
		$this->form_validation->set_rules('calon_lakilaki', 'Calon Lakilaki', 'trim|required');
		$this->form_validation->set_rules('calon_perempuan', 'Calon Perempuan', 'trim|required');
		$this->form_validation->set_rules('tanggal_penikahan', 'Tanggal Penikahan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$judul = [
				'title' => 'Management Surat',
				'sub_title' => 'Dispensasi Nikah'
			];
			$data['data'] = $this->db->get_where('dispensasinikah', ['id' => $id])->row_array();
			$this->load->view('templates/header', $judul);
			$this->load->view('surat/edit_surat_dispensasinikah', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$post = $this->input->post();

			$config['upload_path']          = './uploads/surat_keterangan';
			$config['allowed_types']        = '*';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$data = array('upload_data' => $this->upload->data());
				$post['foto'] = $data['upload_data']['file_name'];
			} 

			$this->db->update('dispensasinikah', $post, ['id' => $id]);
			$this->session->set_flashdata('success', 'Berhasil Diubah!');
			redirect(base_url("dispensasinikah"));
		}
	}

	// Delete action
	public function delete($id) {
		$this->db->delete('dispensasinikah', ['id' => $id]);
		$this->session->set_flashdata('success', 'Berhasil Dihapus!');
		redirect(base_url("dispensasinikah"));
	}
}
