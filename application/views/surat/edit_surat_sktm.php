<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!-- <form id="RegisterValidation" action="" method=""> -->
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <div class="card-content">
						<?php echo form_open_multipart(); ?>
							<h4 class="card-title">Edit Surat SKTM</h4>

							<div class="form-group">
								<label class="control-label">Tanggal</label>
								<input type="date" class="form-control" name="tanggal" value="<?= $data['tanggal']; ?>">
								<?= form_error('tanggal', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="form-group label-floating">
								<label class="control-label">Nama</label>
								<input type="text" class="form-control" name="nama" value="<?= $data['nama']; ?>">
								<?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="form-group label-floating">
								<label class="control-label">Pekerjaan</label>
								<input type="text" class="form-control" name="pekerjaan" value="<?= $data['pekerjaan']; ?>">
								<?= form_error('pekerjaan', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="form-group label-floating">
								<label class="control-label">Alamat</label>
								<textarea class="form-control" name="alamat" cols="30" rows="10"><?= $data['alamat']; ?></textarea>
								<?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>

							</div>

							<div class="form-group label-floating">
								<label class="control-label">Isi Surat</label>
								<textarea class="form-control" name="isi_surat" cols="30" rows="10"><?= $data['isi_surat']; ?></textarea>
								<?= form_error('isi_surat', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="form-group label-floating">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" name="keterangan" cols="30" rows="10"><?= $data['keterangan']; ?></textarea>
								<?= form_error('keterangan', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="form-group">
								<div class="form-group">
										<label class="label-control">Foto</label>
										<div class="fileinput fileinput-new text-center" data-provides="fileinput">
												<div class="fileinput-new thumbnail">
														<!-- <img src="<?= base_url() ?>assets/save.png" alt="..."> -->
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail"></div>
												<div>
														<span class="btn btn-danger btn-file">
																<i class="material-icons">cloud_upload</i>
																<span class="fileinput-new">Select File</span>
																<span class="fileinput-exists">Change</span>
																<input type="file" name="foto" id="foto" />
														</span>
														<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
												</div>
										</div>
								</div>
								<?= form_error('foto', '<small class="text-danger">', '</small>'); ?>
							</div>



							<div class="category form-category">
								<div class="form-footer text-right">

									<button type="submit" class="btn btn-success btn-fill">simpan</button>
								</div>
							</div>
						</form>
                    </div>
                </div>
				
            </div>
        </div>
    </div>
