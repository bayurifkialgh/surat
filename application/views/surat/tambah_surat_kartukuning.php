<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!-- <form id="RegisterValidation" action="" method=""> -->
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <div class="card-content">
											<?php echo form_open_multipart(); ?>
												<h4 class="card-title">Tambah Kartu Kuning</h4>

												<div class="form-group">
													<label class="control-label">Tanggal lahir</label>
													<input type="date" class="form-control" name="tanggal_lahir" value="<?= set_value('tanggal_lahir'); ?>">
													<?= form_error('tanggal_lahir', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">NIK</label>
													<input type="text" class="form-control" name="nik" value="<?= set_value('nik'); ?>">
													<?= form_error('nik', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Nama</label>
													<input type="text" class="form-control" name="nama" value="<?= set_value('nama'); ?>">
													<?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Jenis Kelamin</label>
													<select class="selectpicker" data-style="select-with-transition" title="Pilih Jenis Kelamin" data-size="7" name="jenis_kelamin">
														<option value="Laki-laki" <?= set_value('jenis_kelamin') == 'Laki-laki' ? 'selected' : ''  ?>>Laki-laki</option>
														<option value="Perempuan" <?= set_value('jenis_kelamin') == 'Perempuan' ? 'selected' : ''  ?>>Perempuan</option>
													</select>

													<?= form_error('jenis_kelamin', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Pendidikan</label>
													<input type="text" class="form-control" name="pendidikan" value="<?= set_value('pendidikan'); ?>">
													<?= form_error('pendidikan', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Tahun Lulus</label>
													<input type="number" class="form-control" name="tahun_lulus" value="<?= set_value('tahun_lulus'); ?>">
													<?= form_error('tahun_lulus', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Status</label>
													<input type="text" class="form-control" name="status" value="<?= set_value('status'); ?>">
													<?= form_error('status', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Jurusan</label>
													<input type="text" class="form-control" name="jurusan" value="<?= set_value('jurusan'); ?>">
													<?= form_error('jurusan', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Alamat</label>
													<textarea class="form-control" name="alamat"><?= set_value('alamat'); ?></textarea>
													<?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
												<div class="form-group">
                            <label class="label-control">Foto</label>
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <!-- <img src="<?= base_url() ?>assets/save.png" alt="..."> -->
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div>
                                    <span class="btn btn-danger btn-file">
                                        <i class="material-icons">cloud_upload</i>
                                        <span class="fileinput-new">Select File</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="foto" id="foto" />
                                    </span>
                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                            </div>
                        </div>
													<?= form_error('foto', '<small class="text-danger">', '</small>'); ?>
												</div>
												
												<div class="category form-category">
													<div class="form-footer text-right">

														<button type="submit" class="btn btn-success btn-fill">simpan</button>
													</div>
												</div>
											</form>
                    </div>
                </div>
				
            </div>
        </div>
    </div>
