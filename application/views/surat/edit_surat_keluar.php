<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!-- <form id="RegisterValidation" action="" method=""> -->
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <div class="card-content">
						<?php echo form_open_multipart(); ?>
							<h4 class="card-title">Edit Surat Masuk</h4>

							<div class="form-group">
								<label class="label-control">Nomer Surat</label>
								<input class="form-control" name="nomer_surat" id="nomer_surat" type="text" value="<?= $data['nomer_surat']; ?>" />
							</div>
							<?= form_error('nomer_surat', '<div class="text-danger">', '</div>'); ?>

							<div class="form-group">
								<label class="label-control">Tanggal</label>
								<input class="form-control" name="tanggal" id="tanggal" type="date" value="<?= $data['tanggal']; ?>" />
							</div>
							<?= form_error('tanggal', '<div class="text-danger">', '</div>'); ?>

							<div class="form-group">
								<label class="label-control">Tujuan surat</label>
								<input class="form-control" name="tujuan_surat" id="perihal" type="text" value="<?= $data['tujuan_surat']; ?>" />
							</div>
							<?= form_error('tujuan_surat', '<div class="text-danger">', '</div>'); ?>

							<div class="form-group">
								<label class="label-control">Perihal</label>
								<input class="form-control" name="perihal" id="perihal" type="text" value="<?= $data['perihal']; ?>" />
							</div>
							<?= form_error('perihal', '<div class="text-danger">', '</div>'); ?>

							<div class="form-group">
								<label class="label-control">Keterangan</label>
								<textarea class="form-control" name="keterangan" id="isi_surat" cols="30" rows="10"><?= $data['keterangan']; ?></textarea>
							</div>
							<?= form_error('keterangan', '<div class="text-danger">', '</div>'); ?>

							<div class="form-group">
								<label class="label-control">Tanggal Pengiriman</label>
								<input class="form-control" name="tanggal_pengiriman" id="created_at" type="date" value="<?= $data['tanggal_pengiriman'] ?>" />
							</div>
							<?= form_error('tanggal_pengiriman', '<div class="text-danger">', '</div>'); ?>

							<div class="form-group">
								<div class="form-group">
										<label class="label-control">Foto</label>
										<div class="fileinput fileinput-new text-center" data-provides="fileinput">
												<div class="fileinput-new thumbnail">
														<!-- <img src="<?= base_url() ?>assets/save.png" alt="..."> -->
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail"></div>
												<div>
														<span class="btn btn-danger btn-file">
																<i class="material-icons">cloud_upload</i>
																<span class="fileinput-new">Select File</span>
																<span class="fileinput-exists">Change</span>
																<input type="file" name="foto" id="foto" />
														</span>
														<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
												</div>
										</div>
								</div>
								<?= form_error('foto', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="category form-category">
								<div class="form-footer text-right">

									<button type="submit" class="btn btn-success btn-fill">simpan</button>
								</div>
							</div>
						</form>
                    </div>
                </div>
				
            </div>
        </div>
    </div>
