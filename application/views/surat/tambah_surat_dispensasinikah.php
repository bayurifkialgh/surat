<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!-- <form id="RegisterValidation" action="" method=""> -->
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <div class="card-content">
											<?php echo form_open_multipart(); ?>
												<h4 class="card-title">Tambah Dispensasi Nikah</h4>

												<div class="form-group">
													<label class="control-label">Tanggal Pemohonan</label>
													<input type="date" class="form-control" name="tanggal_pemohonan" value="<?= set_value('tanggal_pemohonan'); ?>">
													<?= form_error('tanggal_pemohonan', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group label-floating">
													<label class="control-label">Nomer Surat</label>
													<input type="text" class="form-control" name="nomer_surat" value="<?= set_value('nomer_surat'); ?>">
													<?= form_error('nomer_surat', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group label-floating">
													<label class="control-label">Calon Laki-laki</label>
													<input type="text" class="form-control" name="calon_lakilaki" value="<?= set_value('calon_lakilaki'); ?>">
													<?= form_error('calon_lakilaki', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group label-floating">
													<label class="control-label">Calon Perempuan</label>
													<input type="text" class="form-control" name="calon_perempuan" value="<?= set_value('calon_perempuan'); ?>">
													<?= form_error('calon_perempuan', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
													<label class="control-label">Tanggal Penikahan</label>
													<input type="date" class="form-control" name="tanggal_penikahan" value="<?= set_value('tanggal_penikahan'); ?>">
													<?= form_error('tanggal_penikahan', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group label-floating">
													<label class="control-label">Alamat</label>
													<textarea class="form-control" name="alamat" cols="30" rows="10"><?= set_value('alamat'); ?></textarea>
													<?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group label-floating">
													<label class="control-label">Keterangan</label>
													<textarea class="form-control" name="keterangan" cols="30" rows="10"><?= set_value('keterangan'); ?></textarea>
													<?= form_error('keterangan', '<small class="text-danger">', '</small>'); ?>
												</div>

												<div class="form-group">
														<div class="form-group">
																<label class="label-control">Foto</label>
																<div class="fileinput fileinput-new text-center" data-provides="fileinput">
																		<div class="fileinput-new thumbnail">
																				<!-- <img src="<?= base_url() ?>assets/save.png" alt="..."> -->
																		</div>
																		<div class="fileinput-preview fileinput-exists thumbnail"></div>
																		<div>
																				<span class="btn btn-danger btn-file">
																						<i class="material-icons">cloud_upload</i>
																						<span class="fileinput-new">Select File</span>
																						<span class="fileinput-exists">Change</span>
																						<input type="file" name="foto" id="foto" />
																				</span>
																				<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
																		</div>
																</div>
														</div>
														<?= form_error('foto', '<small class="text-danger">', '</small>'); ?>
													</div>


												<div class="category form-category">
													<div class="form-footer text-right">

														<button type="submit" class="btn btn-success btn-fill">simpan</button>
													</div>
												</div>
											</form>
                    </div>
                </div>
				
            </div>
        </div>
    </div>
